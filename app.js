const express=require('express')
const app=express()
const port=8080

app.get('/',(req,res)=> {
    res.send("This is my application of CI/CD")
})

app.listen(port,(req,res)=> {
    console.log("app listening on port: ",port)
})
